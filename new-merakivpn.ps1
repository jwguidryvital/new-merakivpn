# new-merakivpn
# -----
# queries user for info and uses that info to set up a new connection to meraki client vpn 

$connectionName = Read-Host -Prompt "Enter name for VPN connection"
$serverAddress = Read-Host -Prompt "Enter hostname or IP address for VPN server"
Write-Host "Enter preshared key"
$preSharedKey = Read-Host 

$existingVPNs = Get-VpnConnection -AllUserConnection

if (($connectionName -in $existingVPNs.Name) -or ($serverAddress -in $existingVPNs.ServerAddress)) {

    while ( !($replaceVPN -in @("Y","y","N","n"))) {

        $replaceVPN = Read-Host -Prompt "One or more VPNs with this name or target server already exist; overwrite? (Y/N)"

    }

}

if ( ($replaceVPN -in @("N","n")) ) {

    Write-Host "Exiting script"

    exit

} else {

    if ($replaceVPN -in @("Y","y")) {

        $existingVPNs | where-object { $_.Name -eq "$connectionName" -OR $_.ServerAddress -eq "$serverAddress" } | Remove-VpnConnection

    }

    Add-VpnConnection -Name "$connectionName" -ServerAddress "$serverAddress" -TunnelType "L2tp" -EncryptionLevel "Optional" -AuthenticationMethod Pap -AllUserConnection -L2tpPsk "$presharedKey" -Force -PassThru

    $existingVPNs = Get-VpnConnection -AllUserConnection

    if ( !($connectionName -in $existingVPNs.Name) ) {

        write-host "Script failed--VPN not created"

    } else {

        Write-Host "Script successful--test VPN to ensure functionality"

    }


}

